name: inverse
layout: true
class: inverse

---

layout: inverse
background-image: url(img/gideon1.webp)
background-size: 100%
background-repeat: no-repeat
background-position: center middle

#

# Developer in a microservice world:

##  what's worthwhile to learn

Jakub Nabrdalik

---
layout: false

# whoami

jakub nabrdalik

solution architect & mentor @bottega

19 years commercial software development

7 years developing microservices exclusively

200+ workshops, talks, etc

details at [solidcraft.eu](https://nabrdalik.dev)

---

layout: false
background-image: url(img/DarkPoster-lowres.png)
background-size: contain
background-repeat: no-repeat
background-position: center middle

---

layout: false
background-image: url(img/aws2000.png)
background-size: contain
background-repeat: no-repeat
background-position: center middle

---


layout: false
background-image: url(img/azure.jpeg)
background-size: contain
background-repeat: no-repeat
background-position: center middle


## Azure - 600+ services


---

layout: false
background-image: url(img/k8sTooComplex.png)
background-size: contain
background-repeat: no-repeat
background-position: center middle

---

# Microservices

Distributed system + Independent deployments

--

DevOps - Developers Operating their own system on Production

--

Large number of devs (for example Allegro: 1500)

--

Independent development 
> small team is responsible for a product

> makes almost ALL decisions

> as little synchronization as possible

--

Usually public or private cloud

---

layout: false
background-image: url(img/romanlegion.png)
background-size: 90%
background-repeat: no-repeat
background-position: center middle

# 100 people working on monolithic system

---

layout: false
background-image: url(img/smallteam.jpeg)
background-size: 70%
background-repeat: no-repeat
background-position: center middle

# 5 people in a complex distributed system

---


layout: false
background-image: url(img/teamtopologies.png)
background-size: contain
background-repeat: no-repeat
background-position: center middle

---

# Analysis

5 devs, PO, QA. Direct access to users.

Productization - a team is usually responsible for "a product". This can be a lot of microservices. 
Since you build a product, you need to understand your clients and talk with them. Be proactive. Learn to say "no".

You need to manage requirements yourself.

--


What to learn:

> How to write Behavioral Specifications (or at least Use Cases)

> How to model processes with sequence diagrams (or block diagrams)

> How to talk with people so they don't get offended, you nerd!

Books

> Behave: The Biology of Humans at Our Best and Worst (Sapolsky)

> The Brain: The Story of You (Eagleman)

> Living Nonviolent Communication (Rosenberg)

---

layout: false
background-image: url(img/microservices.png)
background-size: 70%
background-repeat: no-repeat
background-position: center middle

# Design

---

layout: false
background-image: url(img/modules.png)
background-size: 50%
background-repeat: no-repeat
background-position: center middle

# Design

---

# Design

How to design architecture

What is a module (microservice can be modular!)

How microservices / modules communicate (Query/Command/Event)

How to make REST communication reliable (Outbox pattern)

How to use your Event Bus correctly (Read Models, partitioning, order & delivery guarantees)

How to handle lack of strong consistency (Eventual consistency)

How to compensate operations

*When to split into services based on different non-functional requirements*

> https://www.youtube.com/watch?v=1HJJhGHC2A4

> Designing Event-Driven Systems (Stopford)

> Software Architecture for Developers (Brown)

---

# Implementation

Here everything looks normal (small projects), except

YOU ARE RESPONSIBLE FOR YOUR CODE ON PRODUCTION

What to learn:

- each code branch needs a log, especially if you decide to not do anything
- learn higher order functions to apply logs without introducing mess
- proper business monitoring (Micrometer: decide what to measure)
- proper infra monitoring (watch your thread pools)
- understand your thread model (5 devs in a team)


--

First thing to implement: Tailored Service Template

---

# Deployment

Basic kubernetes knowledge needed: node, pod, service, kubectl

Pipelines - share them across company

Use Continuous Deployment - automatic deploy on each merge to master

Small, frequent deployments (15 on Friday is my personal record)

Remember 
- Big Monoliths == big deployment risk
- Small microservices == small deployment risk

What to learn:
- understand & change your pipeline when needed
- how to rollback to previous version

---

# Operations

What to learn:

- How to setup your dashboards
- How to define alarms
- how to make a thread dump & a memory dump 
- how to debug and see logs (telepresence is a great tool)

And finally, decide how you want to observe & analyse your production

---

# Testing

What to learn:

- There is no such thing as an up-to-date test environment

- Learn to do Behavioral Driven Development

- Use Consumer Driven Contracts (Pact / Spring Cloud Contract, etc.) between teams

- Test your app on production (continuous testing on prod)

---

# Security

Now you are responsible for security. Are you ready for that?

DevSecOps - threat modelling and security experts putting their 5 cents on design

Do not be afraid to ask (or buy) help, but DO NOT IGNORE the problem

Security is a function of probability

Scan docker images for vulnerabilities

Subscribe to your framework newsletter (security updates)

Encrypt data (in flight / in storage)

Use Vaults to keep secrets

---

class: center middle

## Good luck

> jakubn@gmail.com

> twitter: @jnabrdalik 

This presentation is available at https://jakubn.gitlab.io/devinmicroserviceworld
